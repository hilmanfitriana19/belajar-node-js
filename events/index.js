const {EventEmitter} = require("events");   //get the package

const myEventEmitter = new EventEmitter();  //initiate the event

const makeCoffee = (name) =>{               //function to do
    console.log(`Kopi ${name} telah dibuat !`);
};

const makeBill = (price)=>{                 //function to do
    console.log(`Bill sebesar ${price} telah dibuat !`);
};

const onCoffeeOrderedListener = ({name,price}) =>{      //listener for the event
    // using curly to get data from properties of object
    makeCoffee(name);
    makeBill(price);
}

// catch the event
myEventEmitter.on('coffee-order',onCoffeeOrderedListener);

// trigger the event
myEventEmitter.emit('coffee-order',{name:'Tubruk',price:15000});