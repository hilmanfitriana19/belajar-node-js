const fs = require('fs');
const {resolve} = require('path');

// read stream
const readabelStream = fs.createReadStream(resolve(__dirname, 'input.txt'),{
    highWaterMark:15 //nilai buffer nya ambil per berapa byte
});

const writableStream = fs.createWriteStream(resolve(__dirname, 'output.txt'));

readabelStream.on('readable',()=>{
    try {
        stream = readabelStream.read();
        if(stream!=null)
            writableStream.write(`${stream}\n`);
    }catch(error) {
        console.log(error);
        console.log("error read text in file");
    }
});
 
readabelStream.on('end', () => {
    try{
        writableStream.end('');
    }catch(error){
        console.log(error);
        console.log("error read text in file");
    }
});