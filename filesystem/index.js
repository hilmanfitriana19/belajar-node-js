// read file using fs

const fs = require('fs');

// read all data first then return
// const data = fs.readFileSync('note.txt','UTF-8');   // read file

// console.log(data);

// read by stream (part by part)
const readabelStream = fs.createReadStream('article.txt',{
    highWaterMark:10 //nilai buffer nya ambil per berapa byte
});

readabelStream.on('readable',()=>{
    try {
        process.stdout.write(`[${readabelStream.read()}]`);
        // console.log(`[${readabelStream.read()}]`);       //setiap log akan jadi line baru
    }catch(error) {
        console.log(error);
        console.log("error read text in file");
    }
});
 
readabelStream.on('end', () => {
    console.log('Done');
});